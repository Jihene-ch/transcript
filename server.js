const express = require("express"); //for building rest apis
const bodyParser = require("body-parser"); //to parse the request and create the req.body object
const cors = require("cors"); //provides Express middleware to enable CORS with various options.
const app = express(); //create an Express app

//we set origin: http://localhost:8081.
var corsOptions = { 
  origin: "http://localhost:8081"
};

//add body-parser and cors middlewares using app.use()
app.use(cors(corsOptions)); 
// parse requests of content-type - application/json
app.use(bodyParser.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));


// simple route
app.get("/", (req, res) => {
  res.json({ message: "Transcript Generator Tool" });
});
// set port (8080), listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

const db = require("./Backend/models/index.js");
db.sequelize.sync();