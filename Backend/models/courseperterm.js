'use strict';
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class coursePerTerm extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      /*coursePerTerm.belongsTo(models.course,{
        as:'courseId',
        foreignKey:'id'
      });
      coursePerTerm.belongsTo(models.termPerlevel,{
        as:'termperlevelId',
        foreignKey:'id'
      });*/
    }
  }
  coursePerTerm.init({
    weight: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'CoursePerTerm',
  });
  return coursePerTerm;
};