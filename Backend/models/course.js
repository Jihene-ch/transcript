'use strict';
const {
  Model
} = require('sequelize');
const studentCourse=require('./studentcourse');
const courseperterm=require('./courseperterm');

module.exports = (sequelize, DataTypes) => {
  class course extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      /*this.hasMany(studentCourse, {
        as: 'studentcourseId',
        foreignKey: 'id'
      });
      this.hasMany(courseperterm, {
      as: 'coursepertermId',
      foreignKey: 'id'
      });*/
    }
  }
  course.init({
    name: DataTypes.STRING,
    code: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'course',
  });
  return course;
};