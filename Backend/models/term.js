'use strict';
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class term extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      /*term.hasMany(models.termPerLevel, {
        as: 'termperlevelId',
        foreignKey: 'id'
    });*/
    }
  }
  term.init({
    season: DataTypes.STRING,
    year: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'term',
  });
  return term;
};