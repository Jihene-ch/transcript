'use strict';
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class studentCourse extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      /*studentCourse.belongsTo(models.course, {
        as: 'courseId',
        foreignKey: 'id'
      });
      studentCourse.belongsTo(models.studentTerm,{
        as:'studenttermId',
        foreignKey:'id'
      })*/
    }
  }
  studentCourse.init({
  }, {
    sequelize,
    modelName: 'studentCourse',
  });
  return studentCourse;
};