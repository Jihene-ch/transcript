'use strict';
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class level extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      /*level.hasMany(models.termPerLevel, {
        as: 'termperlevelId',
        foreignKey: 'id'  
    });*/
    }
  }
  level.init({
    level: DataTypes.ENUM('Freshman','Sophomore', 'Junior', 'Senior', 'Final','Bachelor'),
    major: DataTypes.ENUM('None','SE','RE','CSE'),
    group: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'level',
  });
  return level;
};