'use strict';
const { user } = require('pg/lib/defaults');
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class student extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      /*student.belongsTo(models.user, {
        as: 'userId',
        foreignKey: 'id'
    });*/
    }
  }
  student.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    studentId: DataTypes.STRING,
    birthday: DataTypes.STRING,
    cin: DataTypes.STRING,
    arabicName: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'student',
  });
  return student;


};

