'use strict';
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class studentTerm extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      /*studentTerm.hasMany(models.studentCourse,{
        as:'studentcourseId',
        foreignKey:'id'
      });
      studentTerm.belongsTo(models.termPerLevel,{
        as:'termperlevelId',
        foreignKey:'id'
      });
      studentTerm.belongsTo(models.user,{
        as:'userId',
        foreignKey:'id'
      })*/
    }
  }
  studentTerm.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true}
  }, {
    sequelize,
    modelName: 'studentTerm',
  });
  return studentTerm;
};